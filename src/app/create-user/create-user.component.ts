import { Component, OnInit } from '@angular/core';
import{Http,Response,Headers} from'@angular/http';
import { UserManagementServices } from '../services/userManagement.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  newuser:Object={};
  msg:string="New Product Has Been Added";
  isAdded:boolean=false;

  constructor(private http:Http,private services:UserManagementServices) { }

adduser=function(user){
  this.newuser={
    "name": user.name,
    "email": user.email,
    "age":user.age,
    "country":user.country
  }
  this.services.addNewUser(this.newuser).subscribe((res:Response)=>{
    console.log(res);
    this.isAdded=true;
  })
}

  ngOnInit() {
  }

}
