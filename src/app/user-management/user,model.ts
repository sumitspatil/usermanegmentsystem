export class UserModel{
    name:string;
    id:number;
    email:string;
    country:string;
    constructor(i:number,n:string,e:string,c:string){
        this.id=i;
        this.name=n;
        this.email=e;
        this.country=c;
    }
}