
import { Component, OnInit } from '@angular/core';
import{Router} from"@angular/router";
import{Http,Response,Headers} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { UserManagementServices } from '../services/userManagement.service';
import{UserModel} from"./user,model";

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css',]
})
export class UserManagementComponent implements OnInit {

  user:UserModel[]=new Array(0);
  selectedId:any;
  constructor(private route:Router,private http:Http, private service:UserManagementServices) {
   }
   private headers= new Headers({'Content-Type':'application/json'});

getUser(){
  this.service.fetchData().subscribe(data=>this.user=data);
}//to  call fetchdata function from services
update(){
  this.route.navigate(["updateUser",this.selectedId]);
}
createUser(){
  this.route.navigate(["createUser"]);
}

selctId(id){
this.selectedId=id;
}//to get id of user selected from table

deleteUser(){
  if (confirm("Are You Sure ?")) {
    const url = 'http://18.188.108.212:8888/users/'+this.selectedId;
    return this.http.delete(url, { headers: this.headers }).subscribe(() => {
      this.getUser();
    });
  }
}//end of delete user function



  ngOnInit() {
    this.getUser();
  }

}

