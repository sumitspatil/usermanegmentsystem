import {Injectable} from"@angular/core";
import{Http,Response} from "@angular/http";
import 'rxjs/add/operator/toPromise';
@Injectable()

export class UserManagementServices{
    users=[];

    constructor(private http:Http){}
    fetchData(){

        return this.http.get("http://18.188.108.212:8888/users").map((res:Response)=>{
            return res.json()
            //this.users=res.json().users;
        });
    }

    addNewUser=function(user){
       return this.http.post("http://18.188.108.212:8888/users/",user);
    }

    deleteUserById(id){
       return this.http.delete("http://18.188.108.212:8888/users/",id);
    }

}
