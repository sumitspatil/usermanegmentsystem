import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{RouterModule,Routes} from '@angular/router'
import{FormsModule} from "@angular/forms";
import{HttpModule} from'@angular/http';
import { AppComponent } from './app.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UserManagementServices } from './services/userManagement.service';

const route:Routes=[{path:"",component:UserManagementComponent},
{path:"updateUser/:id",component:UpdateUserComponent},
{path:"createUser",component:CreateUserComponent}];
@NgModule({
  declarations: [
    AppComponent,
    UserManagementComponent,
    UpdateUserComponent,
    CreateUserComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(route),FormsModule,HttpModule  ],
  providers: [UserManagementServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
