import { Component, OnInit } from '@angular/core';
import{Http,Response,Headers} from'@angular/http';
import{ActivatedRoute,Router} from"@angular/router";
import 'rxjs/add/operator/toPromise';
import { Route } from '@angular/router/src/config';
import { UserManagementServices } from '../services/userManagement.service';
@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  id:number;
  data:object={};
  users=[];
  userObject:Object={};
  exist=false;
  private headers = new Headers({'Content-Type':'application/json'});

  constructor(private router:Router,private route:ActivatedRoute,private http:Http,private services:UserManagementServices) { }

  updateUser(user){
    this.userObject={
      "name":user.name,
      "email":user.email,
      "age":user.age,
      "country":user.country
    }
    const url = 'http://18.188.108.212:8888/users/'+this.id;
    this.http.put(url,JSON.stringify(this.userObject), {headers:this.headers}).toPromise().then(()=>{
      this.router.navigate(["/"]);
    })
  }
  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.id=Number(params['id']);//that plus sign will going to parse this value into the integer
    });
    this.http.get("http://18.188.108.212:8888/users").subscribe((res:Response)=>{
      this.users=res.json();
      for(var i=0; i<this.users.length; i++){
        if(parseInt(this.users[i].id)===this.id){
          this.exist=true;
          this.data=this.users[i];
          break;
        }
        else{
          this.exist=false;
          }
}
})

}
}
