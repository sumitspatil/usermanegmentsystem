# UserManagementSystem

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

##Step to Run 
1. Install Node.

2. Run "npm install" on command prompt.

3. Run Json Server  .
   3.1 Browse the path of user.json file into command prompt.
   
   3.2 run "json-server -p 8888 user.json" to start JsonServer

   4.run "npm start" on a new command promt.
